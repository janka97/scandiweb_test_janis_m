<?php 
$title = 'Product list';
$page = 'Product list';
require 'parts/header.php';
include('../aos/Products.php');

$products = new Products;
$dvd = new dvd;
$book = new book;
$furniture = new furniture;
if (isset($_POST["product"])) {  
	$products->delete($_POST['product']);

}
?>
<section>
<form method="post">
	<div class="delete-bar">
		<div class="left">
			<label>Select All</label><input type="checkbox" onclick="toggleall(this)">
		</div>
		<input type="submit" name="submit" class="btn right" value="Delete">
		<div class="fix"></div>
	</div>
<?php

$allproducts = $products->show();
if ($allproducts->num_rows > 0) { 

$dvds = $dvd->showdvd();
	while ($results = mysqli_fetch_object($dvds)) {
	?>
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 product-box">
		<div class="product-innerbox">
			<input type="checkbox" class="product-checkbox" name="product[]" onclick="togle()" value="<?php echo $results->id ?>">
			<p>Name: <?php echo $results->name ?></p>
			<p>SKU: <?php echo $results->sku ?></p>
			<p>Price: <?php echo $results->price ?> $</p>
			<p>Disk Size: <?php echo $results->size ?> MB</p>
		</div>
	</div>
	<?php
	 
}

$books = $book->showbook();
	while ($results = mysqli_fetch_object($books)) {
	?>
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 product-box">
		<div class="product-innerbox">
			<input type="checkbox" class="product-checkbox" name="product[]" onclick="togle()" value="<?php echo $results->id ?>">
			<p>Name: <?php echo $results->name ?></p>
			<p>SKU: <?php echo $results->sku ?></p>
			<p>Price: <?php echo $results->price ?> $</p>
			<p>Disk Size: <?php echo $results->weight ?> Kg</p>
		</div>
	</div>
	<?php
	
}

$furnitures = $furniture->showfurniture();

	while ($results = mysqli_fetch_object($furnitures)) {
	?>
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 product-box">
		<div class="product-innerbox">
			<input type="checkbox" class="product-checkbox" name="product[]" onclick="togle()" value="<?php echo $results->id ?>">
			<p>Name: <?php echo $results->name ?></p>
			<p>SKU: <?php echo $results->sku ?></p>
			<p>Price: <?php echo $results->price ?> $</p>
			<p  title='Height x width x lenght'><?php echo 'Dimension:' . $results->height . ' x ' . $results->width . ' x ' . $results->lenght ?></p>
		</div>
	</div>
	<?php
	} 
} else {
	echo 'There are no products to show!';
}
?>

</form>
</section>
<?php
include 'parts/footer.php';
?>