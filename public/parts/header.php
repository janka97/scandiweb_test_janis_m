<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Scandiweb | <?php echo $title ?></title>
    <link href="css/style.min.css" rel='stylesheet' type='text/css' />   
</head>
<body>
    <nav>
        <div class="container">
            <ul>
                <li class="<?php echo $page == "Product list" ? "active" : "" ?>"><a href="products.php">Product list</a></li>
                <li class="<?php echo $page == "Products add" ? "active" : "" ?>"><a href="addproducts.php">Product add</a></li>
            </ul>
        </div>
    </nav>
    <div class="fix"></div>
    <div class="container body-bg">
