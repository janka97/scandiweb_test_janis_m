<?php 
$title = 'Product add';
$page = 'Products add';
require 'parts/header.php';
include('../aos/Products.php');

$data = new Products;
$msg = '';
if (isset($_POST["submit"])) {
	$insert_data = array(
		'sku' => mysqli_real_escape_string($data->con, $_POST['sku']),
		'name' => mysqli_real_escape_string($data->con, $_POST['name']),
		'price' => mysqli_real_escape_string($data->con, $_POST['price']),
		'type' => mysqli_real_escape_string($data->con, $_POST['type']),
		'size' => mysqli_real_escape_string($data->con, $_POST['size']),
		'weight' => mysqli_real_escape_string($data->con, $_POST['weight']),
		'height' => mysqli_real_escape_string($data->con, $_POST['height']),
		'width' => mysqli_real_escape_string($data->con, $_POST['width']),
		'lenght' => mysqli_real_escape_string($data->con, $_POST['lenght']),
	);
	if ($data->insert($insert_data)) {
		$msg = "Dati ievietoti datubaze!";
	}
}
?>
<section class="offset-2">
	<form class="form-group" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<div class="form-input">
			<label>SKU: </label><br>
			<input type="text" name="sku" autofocus required>
		</div>
		<div class="form-input">
			<label>Name: </label><br>
			<input type="text" name="name" required>
		</div>
		<div class="form-input">
			<label>Price: </label><br>
			<input type="number" step="any" min="0" name="price" required>
		</div>
		<div class="form-input">
		<label>Type:</label>
			<select id="typeselect" onchange="selecttype()" name="type">
					<option name="type" value="DVD">DVD</option>
					<option name="type" value="Book">Book</option>
					<option name="type" value="Furniture">Furniture</option>
			</select>
		</div>
		<div id="dvd" class="">
				<div class="form-input">
					<label>Size: </label><br>
					<input id="dvdi" type="number" step="any" min="0" name="size" required> MB
					<P>Please provide disc size in megabytes!</P>
				</div>
		</div>
		<div id="book" class="hidden">
				<div class="form-input">
					<label>Weight: </label><br>
					<input id="booki" type="number" step="any" min="0" name="weight"> Kg
					<p>Please provide disc size in kilograms!</P>
				</div>
		</div>
		<div id="furniture" class="hidden">
				<div class="form-input">
					<div class="form-input">
						<label>Height: </label><br>
						<input id="furnitureh" type="number" step="any" min="0" name="height"> cm
						<p>Please provide furnitures height in centimetres!</P>
					</div><hr><div class="fix"></div>
					<div class="form-input">
						<label>Width(depth): </label><br>
						<input id="furniturew" type="number" step="any" min="0" name="width"> cm
						<p>Please provide furnitures width/depth in centimetres!</P>
					</div><hr><div class="fix"></div>
					<div class="form-input">
						<label>Lenght: </label><br>
						<input id="furniturel" type="number" step="any" min="0" name="lenght"> cm
						<p>Please provide furnitures lenght in centimetres!</P>
					</div>
				</div>
				</div>
		<input type="submit" name="submit" value="Submit" class="btn">
		<?php echo $msg; ?>
	</form>
</section>

<?php
include 'parts/footer.php';
?>