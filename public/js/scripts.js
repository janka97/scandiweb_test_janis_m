/**
 * Changes type content 
 */
function selecttype() {
    type = document.getElementById("typeselect").value;
    dvdi = document.getElementById("dvdi");
    dvd = document.getElementById("dvd");
    book = document.getElementById("book");
	booki = document.getElementById("booki");
	furniture = document.getElementById("furniture");
	furnitureh = document.getElementById("furnitureh");
	furniturew = document.getElementById("furniturew");
	furniturel = document.getElementById("furniturel");

    if(type == "DVD") {
	    dvd.classList.remove("hidden");
	    dvdi.required = !0;
	    book.classList.add("hidden");
	    booki.removeAttribute("required");
	    furniture.classList.add("hidden");
	    furnitureh.removeAttribute("required");
	    furniturew.removeAttribute("required");
	    furniturel.removeAttribute("required");
    }
    if(type == "Book") {
	    dvd.classList.add("hidden");
	    dvdi.removeAttribute("required");
	    book.classList.remove("hidden");
	    booki.required = !0;
	    furniture.classList.add("hidden");
	    furnitureh.removeAttribute("required");
	    furniturew.removeAttribute("required");
	    furniturel.removeAttribute("required");
    }
    if(type == "Furniture") {
	    dvd.classList.add("hidden");
	    dvdi.removeAttribute("required");
	    book.classList.add("hidden");
	    booki.removeAttribute("required");
	    furniture.classList.remove("hidden");
	    furnitureh.required = !0;
	    furniturew.required = !0;
	    furniturel.required = !0;
    }
    
}

/**
 * To toggle all checkboxes for product deleting
 */
function toggleall(source) {
	checkboxes = document.getElementsByName('product[]');
	for( i=0; i < checkboxes.length; i++) {
		checkboxes[i].checked = source.checked;
	}
}
