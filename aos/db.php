<?php
/**
 * Starts conection to database and ends it.
 * @author Janis <janismucins97@gmail.com>
 */
class Databases {
	public $con;
	public $error;

	public function __construct() {
		$this->con = mysqli_connect("localhost", "id3540678_scandiweb", "liene1804", "id3540678_scandiweb");
		if (!$this->con) {
		echo 'Database Connection Error' . mysqli_connect_error($this->con);
		}
	}

	public function __destruct() {
		mysqli_close($this->con);
	}
}
