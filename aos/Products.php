<?php

include 'db.php';

/**
 * Execute database querys to show products, insert them and delete.
 * @author Janis <janismucins97@gmail.com>
 * 
 */
class Products extends Databases
{
	/**
	 * query data from database
	 * @return $resuls
	 */
	public function show()
	{
		if ($result = mysqli_query($this->con, "SELECT * FROM products")) {
			return $result;
		} else {
		echo "Error: " . $this->con->error;
		}
	}
	 
	/**
	 * Inserts values into database
	 * @param array $insert 
	 */
	public function insert($insert) {
		$query = "INSERT INTO products (" . implode(",", array_keys($insert)) . ") VALUES ('" . implode("','", array_values($insert)) . "')";
		if(mysqli_query($this->con, $query)) {
			echo "<script>alert('Product successfully inserted');</script>";
		} else {
			echo mysqli_error($this->con);
		}
	}

	/**
	 * Deletes one or multiple files.
	 * @param data $product_id 
	 */
	public function delete($product_id) {
		$delete = implode(',', $product_id);
		$query = "DELETE FROM products WHERE id IN (" . $delete . ")";
		if(mysqli_query($this->con, $query)) {
			if (strpos($delete, ',') !== false) {
			echo "<script>alert('Products successfully deleted');</script>";
			} else {
			echo "<script>alert('Product successfully deleted');</script>";
			}
		} else {
			echo mysqli_error($this->con);
		}
	}
}

class dvd extends Databases
{
	public function showdvd()
	{
		if ($dvd = mysqli_query($this->con, "SELECT * FROM products WHERE type = 'DVD' ")) {
			return $dvd;
		} else {
		echo "Error: " . $this->con->error;
		}
	}
}

class book extends Databases
{
	public function showbook()
	{
		if ($dvd = mysqli_query($this->con, "SELECT * FROM products WHERE type = 'book' ")) {
			return $dvd;
		} else {
		echo "Error: " . $this->con->error;
		}
	}
}

class furniture extends Databases
{
	public function showfurniture()
	{
		if ($dvd = mysqli_query($this->con, "SELECT * FROM products WHERE type = 'Furniture' ")) {
			return $dvd;
		} else {
		echo "Error: " . $this->con->error;
		}
	}
}
